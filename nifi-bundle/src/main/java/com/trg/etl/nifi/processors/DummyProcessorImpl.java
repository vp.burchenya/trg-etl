package com.trg.etl.nifi.processors ;

import org.apache.nifi.logging.ComponentLog ;
import org.apache.nifi.processor.AbstractProcessor ;
import org.apache.nifi.processor.ProcessorInitializationContext ;
import org.apache.nifi.processor.ProcessContext ;
import org.apache.nifi.processor.ProcessSession ;
import org.apache.nifi.processor.Relationship ;
import org.apache.nifi.processor.exception.ProcessException ;
import org.apache.nifi.processor.util.StandardValidators ;
import org.apache.nifi.components.PropertyDescriptor ;
import org.apache.nifi.expression.ExpressionLanguageScope ;
import org.apache.nifi.flowfile.FlowFile ;
import org.apache.nifi.annotation.behavior.* ;
import org.apache.nifi.annotation.lifecycle.OnScheduled ;
import org.apache.nifi.annotation.documentation.Tags ;
import org.apache.nifi.annotation.documentation.CapabilityDescription ;

import java.util.Collections ;
import java.util.List ;
import java.util.HashSet ;
import java.util.Set ;


@SupportsBatching
// @InputRequirement(Requirement.INPUT_REQUIRED)
// @EventDriven
// @DynamicProperty
@Tags({"dummy", "trg"})
@CapabilityDescription("Dummy processor")
@ReadsAttributes({@ReadsAttribute(attribute="last_value", description="")})
@WritesAttributes({@WritesAttribute(attribute="last_value", description="")})
public class DummyProcessorImpl extends AbstractProcessor {

    private List<PropertyDescriptor> propertyDescriptors ;
    private Set<Relationship> relations ;
    private final Relationship DUMMY_REL = new Relationship.Builder()
        .name(REL_DUMMY)
        .description("Dummy relationship")
        .build() ;

    public static final String REL_DUMMY = "DUMMY_REL" ;

    @Override
    public void onTrigger(final ProcessContext ctx, final ProcessSession session)
    throws ProcessException {
        final ComponentLog log = getLogger() ;
        FlowFile ff = session.get() ;
        log.debug("Process {}", new Object[]{ff}) ;

        if (ff != null) {
            String prop = ctx.getProperty("DUMMY_PROPERTY").evaluateAttributeExpressions(ff).getValue() ;
            final String lastValue = ff.getAttribute("last_value") ;
            ff = session.putAttribute(ff, "last_value", lastValue + prop + lastValue) ;
            session.getProvenanceReporter().modifyAttributes(ff) ;
            session.transfer(ff, DUMMY_REL) ;
        }
    }

    @OnScheduled
    public void onScheduled(final ProcessContext ctx) {}

    @Override
    protected void init(ProcessorInitializationContext iCtx) {
        super.init(iCtx) ;
        propertyDescriptors = Collections.singletonList(
            new PropertyDescriptor.Builder()
            .name("DUMMY_PROPERTY")
            .displayName("Dummy property")
            .description("Just a dummy property")
            .required(false)
            .expressionLanguageSupported(ExpressionLanguageScope.FLOWFILE_ATTRIBUTES)
            .dynamic(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build()
        ) ;
        relations = Collections.unmodifiableSet(
            new HashSet<Relationship>() {{
                add(DUMMY_REL) ;
            }}
        ) ;
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relations ;
    }
    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return propertyDescriptors ;
    }

}
