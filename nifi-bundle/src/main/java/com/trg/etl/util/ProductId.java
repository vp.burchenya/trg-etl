package com.trg.etl.util ;

import org.jetbrains.annotations.NotNull ;

import static java.nio.charset.StandardCharsets.UTF_8 ;
import java.security.NoSuchAlgorithmException ;
import java.util.UUID ;
import java.nio.ByteBuffer ;
import java.security.MessageDigest ;


public class ProductId {

    private static final Byte VERSION = 4 ;

    @NotNull
    public static UUID calculateProductId(@NotNull String source) throws NoSuchAlgorithmException {

        assert 1 <= VERSION && VERSION <= 15 ;

        ByteBuffer bb = ByteBuffer.wrap(
            MessageDigest.getInstance("MD5").digest(source.getBytes(UTF_8))
        ) ;

        // Set version
        long most  = bb.getLong() & 0xffffffffffff0fffL | VERSION << 12 ;  // OR 0x4000L for V4
        // Set variant 2 (RFC-4122)
        long least = bb.getLong() & 0x3fffffffffffffffL | 0x8000L << 48 ;

        return new UUID(most, least) ;
    }
}
