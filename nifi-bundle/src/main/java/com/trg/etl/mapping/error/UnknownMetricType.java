package com.trg.etl.mapping.error ;


public class UnknownMetricType extends MappingError {
    public UnknownMetricType() {
        super() ;
    }

    public UnknownMetricType(String message) {
        super(message) ;
    }
}
