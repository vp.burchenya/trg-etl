package com.trg.etl.mapping ;

import com.trg.etl.model.metric.Unit ;

import java.util.HashMap ;
import java.util.Map ;


public interface IUnitMapping {
    Map<String, Unit> MAPPING = new HashMap<String, Unit>() {{
        // FIXME: Mapper work
        put("POUNDS", Unit.POUND) ;
        put("LB", Unit.POUND) ;
        put("LBR", Unit.POUND) ;
        put("METER", Unit.METER) ;
        put("INCHES", Unit.INCH) ;
        put("INH", Unit.INCH) ;
        put("\"", Unit.INCH) ;
        put("OUNCES", Unit.OUNCE) ;
        put("FEET", Unit.FOOT) ;
        put("KILOGRAM", Unit.KG) ;
        put("KILOGRAMS", Unit.KG) ;
    }} ;
}
