package com.trg.etl.mapping.error ;


public class MappingError extends Exception {
    public MappingError() {
        super() ;
    }

    public MappingError(String message) {
        super(message) ;
    }
}
