package com.trg.etl.mapping ;

import com.trg.etl.model.Category ;
import com.trg.etl.rr.to.product.Categories ;

import org.jetbrains.annotations.NotNull ;
import org.jetbrains.annotations.Nullable ;

import java.net.MalformedURLException ;
import java.net.URL ;
import java.util.ArrayList ;
import java.util.List ;
import java.util.stream.IntStream ;


public interface CategoryMapper {
    @NotNull
    static List<Category> convertCategory(@NotNull Categories.Category category) throws MalformedURLException {

        List<String> names = category.getNames() ;
        List<String> ids = category.getIds() ;
        List<String> hrefs = category.getLinks() ;
        List<URL> urls = new ArrayList<>() ;

        for (String href: hrefs) urls.add(new URL(href)) ;

        int max = Math.max(Math.max(names.size(), ids.size()), hrefs.size()) ;

        // Replace this with functional reduce
        final List<Category> cat = new ArrayList<>() ;
        IntStream.range(0, max).forEach(i -> {
            String name = getOrNull(names, i) ;
            String id = getOrNull(ids, i) ;
            URL url = getOrNull(urls, i) ;
            cat.add(new Category(name, id, url)) ;
        }) ;

        return cat ;
    }

    // TODO: Move to `Util`
    @Nullable
    static <T> T getOrNull(@NotNull List<T> list, Integer i) {
        return list.size() > i ? list.get(i) : null ;
    }
}
