package com.trg.etl.mapping.error ;


public class UnknownConditionType extends MappingError {
    public UnknownConditionType() {
        super() ;
    }

    public UnknownConditionType(String message) {
        super(message) ;
    }
}
