package com.trg.etl.mapping.error ;


public class UnknownDimensionType extends MappingError {
    public UnknownDimensionType() {
        super() ;
    }

    public UnknownDimensionType(String message) {
        super(message) ;
    }
}
