package com.trg.etl.mapping.error ;


public class UnknownIdentifierType extends MappingError {
    public UnknownIdentifierType() {
        super() ;
    }

    public UnknownIdentifierType(String message) {
        super(message) ;
    }
}
