package com.trg.etl.mapping.error ;


public class MultipleBaseIdentifiers extends MappingError {
    public MultipleBaseIdentifiers() {
        super() ;
    }

    public MultipleBaseIdentifiers(String message) {
        super(message) ;
    }
}
