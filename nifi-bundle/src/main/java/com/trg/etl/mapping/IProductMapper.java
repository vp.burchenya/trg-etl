package com.trg.etl.mapping ;

import com.trg.etl.mapping.error.* ;
import com.trg.etl.model.Product ;
import com.trg.etl.model.identifier.* ;
import com.trg.etl.model.Condition ;
import com.trg.etl.model.metric.*;
import com.trg.etl.rr.to.product.ImageSet ;
import com.trg.etl.rr.to.product.Metric ;
import com.trg.etl.rr.to.product.ProductTo ;
import com.trg.etl.util.ProductId;

import org.jetbrains.annotations.NotNull ;
import org.jetbrains.annotations.Nullable ;
import org.mapstruct.* ;
import org.mapstruct.factory.Mappers ;

import java.net.MalformedURLException ;
import java.security.NoSuchAlgorithmException ;

import java.net.URL ;
import java.util.* ;


@Mapper(
    nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL,
    uses = CategoryMapper.class
)
public interface IProductMapper {

    IProductMapper INSTANCE = Mappers.getMapper(IProductMapper.class) ;

    // Mapping allows multiple sources as arguments

    @Mapping(qualifiedByName = "Join", source = "description.long", target = "longDescription")
    @Mapping(qualifiedByName = "Join", source = "description.short", target = "shortDescription")
    @Mapping(target = "identifiers")
    @Mapping(source = "to", target = "productId")
    @Mapping(source = "categories.primary", target = "primaryCategory")
    @Mapping(source = "categories.secondary", target = "secondaryCategories")
    @Mapping(source = "imageSet", target = "mainImage", resultType = URL.class)
    @Mapping(source = "imageSet", target = "subImages")
    @Mapping(source = "imageSet.video", target = "videos")
    @Mapping(source = "imageSet.stereo", target = "stereoImages")
    Product toModel(ProductTo to) throws MappingError, MalformedURLException ;

    @Named("Join")
    static String join(List<String> source) {
        if (source == null || source.isEmpty())
            return null ;
        else
            return String.join("\n", source) ;
    }

    @NotNull
    static Condition mapCondition(@NotNull final String condition) throws UnknownConditionType {
        try {
            return Condition.valueOf(condition.toUpperCase()) ;
        } catch (IllegalArgumentException e) {
            throw new UnknownConditionType(condition) ;
        }
    }

    @NotNull
    static URL mapUrl(final String url) throws MalformedURLException {
        return new URL(url) ;
    }

    @Nullable
    static URL mapImage(@NotNull final ImageSet imageSet) throws MalformedURLException {
        List<String> main = imageSet.getMain() ;
        if (main.size() > 0)
            return new URL(main.get(0)) ;
        else
            return null ;
    }

    @NotNull
    static List<URL> mapImages(@NotNull final ImageSet imageSet) throws MalformedURLException {
        List<String> main = imageSet.getMain() ;
        List<String> sub = imageSet.getSub() ;
        List<String> resultStr = new ArrayList<>() ;
        if (main.size() > 1)
            resultStr.addAll(
                main.subList(1, main.size())
            ) ;
        if (sub != null && sub.size() > 1)
            resultStr.addAll(sub) ;

        List<URL> result = new ArrayList<>() ;
        for (String href : resultStr)
            result.add(new URL(href)) ;
        return result ;
    }

    // TODO: Ignore case
    @Nullable
    static UUID generateProductId(@NotNull ProductTo to) throws UnknownIdentifierType, MultipleBaseIdentifiers, NoSuchAlgorithmException {
        String identifierName = to.getBaseIdentifierTypeName() ;
        if (identifierName != null) {
            List<String> identifierValues = to.getIdentifiers().get(identifierName);
            if (identifierValues != null) {
                if (identifierValues.size() != 1)
                    throw new MultipleBaseIdentifiers() ;
                else {
                    BaseIdentifier identifier = IdentifierFabric.build(identifierName, identifierValues.get(0)) ;
                    if (identifier == null)
                        throw new UnknownIdentifierType(identifierName) ;
                    return ProductId.calculateProductId(to.getProvider() + identifier) ;
                }
            }
        }
        return null ;
    }

    @NotNull
    static Set<BaseIdentifier> mapIdentifiers(@NotNull Map<String, List<String>> identifiers) throws UnknownIdentifierType {
        Set<BaseIdentifier> result = new HashSet<>() ;
        for (Map.Entry<String, List<String>> identifierItem : identifiers.entrySet()) {
            String identifierName = identifierItem.getKey() ;
            for (String identifierValue: identifierItem.getValue() ) {
                BaseIdentifier identifier = IdentifierFabric.build(identifierName, identifierValue) ;
                if (identifier != null)
                    result.add(identifier) ;
                else
                    throw new UnknownIdentifierType(identifierName) ;
            }
        }
        return result ;
    }

    @NotNull
    static Dimensions mapDimensions(@NotNull final Map<@NotNull String, @Nullable Metric> dimensions) throws
        UnknownMetricType,
        UnknownDimensionType {

        Dimensions result = new Dimensions() ;

        for (Map.Entry<String, Metric> item : dimensions.entrySet()) {
            Unit unit ;
            Metric metric = item.getValue() ;

            if (metric == null)
                continue ;

            String dimensionName = item.getKey().toLowerCase() ;
            String unitName = metric.getUnit().toUpperCase() ;

            try {
                unit = Unit.valueOf(unitName) ;
            } catch (IllegalArgumentException ex) {
                unit = IUnitMapping.MAPPING.get(unitName) ;
                if (unit == null)
                    throw new UnknownMetricType(unitName) ;
            }

            switch (dimensionName) {
                case "length":
                case "depth":
                    result.setLength(new Length(metric.getValue(), unit)) ;
                    break ;
                case "height":
                    result.setHeight(new Height(metric.getValue(), unit)) ;
                    break ;
                case "width":
                    result.setWidth(new Width(metric.getValue(), unit)) ;
                    break ;
                case "weight":
                    result.setWeight(new Weight(metric.getValue(), unit)) ;
                    break ;
                default:
                    throw new UnknownDimensionType(dimensionName) ;
            }
        }

        return result ;
    }
}
