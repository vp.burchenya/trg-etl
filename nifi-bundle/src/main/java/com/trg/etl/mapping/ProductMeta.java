package com.trg.etl.mapping ;

import com.trg.etl.model.identifier.* ;

import java.util.Collections ;
import java.util.HashMap ;
import java.util.Map ;


public class ProductMeta {

    public static String MIME_TYPE = "application/vnd.gotrg.reaper.product-v0+json" ;

    public static Map<String, Class<? extends BaseIdentifier>>
    PROVIDER_BASE_IDENTIFIERS = Collections.unmodifiableMap(
        new HashMap<String, Class<? extends BaseIdentifier>>() {{
            put("walmart.com", SKU.class) ;
            put("lowes.com", ItemNumber.class) ;
        }}
    ) ;
}
