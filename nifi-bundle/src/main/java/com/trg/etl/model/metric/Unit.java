package com.trg.etl.model.metric ;

import com.trg.etl.mapping.IUnitMapping;
import org.jetbrains.annotations.NotNull ;


public enum Unit {
    INCH,
    POUND,
    CM,
    METER,
    FOOT,
    OUNCE,
    KG ;

    @NotNull
    public static Unit valueOfIgnoreCase(@NotNull final String name) {
        try {
            return Unit.valueOf(name.toUpperCase()) ;
        } catch (IllegalArgumentException ex) {
            Unit result = IUnitMapping.MAPPING.get(name.toUpperCase()) ;
            if (result == null)
                throw new IllegalArgumentException() ;
            else
                return result ;
        }
    }
}
