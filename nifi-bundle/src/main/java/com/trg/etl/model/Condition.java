package com.trg.etl.model ;

import org.jetbrains.annotations.NotNull ;


public enum Condition {
    NEW(0),
    USED(1),
    REFURBISHED(2) ;

    private final Integer value ;

    public Integer getValue() {
        return value ;
    }

    private Condition(final Integer value) {
        this.value = value ;
    }
}
