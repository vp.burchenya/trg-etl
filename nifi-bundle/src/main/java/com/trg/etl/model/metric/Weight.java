package com.trg.etl.model.metric ;

import org.jetbrains.annotations.NotNull;


public class Weight extends Metric {
    public Weight(@NotNull Double value, @NotNull Unit unit) {
        super(value, unit) ;
    }
}
