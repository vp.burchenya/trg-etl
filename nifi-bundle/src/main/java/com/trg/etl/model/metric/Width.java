package com.trg.etl.model.metric;

import org.jetbrains.annotations.NotNull ;


public class Width extends Metric {
    public Width(@NotNull Double value, @NotNull Unit unit) {
        super(value, unit) ;
    }
}
