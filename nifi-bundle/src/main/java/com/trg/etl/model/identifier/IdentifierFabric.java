package com.trg.etl.model.identifier ;

import org.jetbrains.annotations.NotNull ;
import org.jetbrains.annotations.Nullable ;


public class IdentifierFabric {
    @Nullable
    public static BaseIdentifier build(@NotNull String identifierType, String value) {
        switch (identifierType.toLowerCase()) {
            case "upc":
                return new UPC(value) ;
            case "sku":
                return new SKU(value) ;
            case "item_number":
                return new ItemNumber(value) ;
            default:
                return null ;
        }
    }
}
