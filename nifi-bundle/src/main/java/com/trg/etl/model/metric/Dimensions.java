package com.trg.etl.model.metric ;


public class Dimensions {
    private Height height ;
    private Length length ;
    private Weight weight ;
    private Width width ;

    public Height getHeight() {
        return height ;
    }
    public void setHeight(Height height) {
        this.height = height ;
    }

    public Length getLength() {
        return length ;
    }
    public void setLength(Length length) {
        this.length = length ;
    }

    public Weight getWeight() {
        return weight ;
    }
    public void setWeight(Weight weight) {
        this.weight = weight ;
    }

    public Width getWidth() {
        return width ;
    }
    public void setWidth(Width width) {
        this.width = width ;
    }
}
