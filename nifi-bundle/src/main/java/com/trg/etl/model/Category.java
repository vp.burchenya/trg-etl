package com.trg.etl.model ;

import org.jetbrains.annotations.Nullable ;

import java.net.URL ;


public class Category {

    @Nullable
    private String name ;
    @Nullable
    private String id ;
    @Nullable
    private URL url ;

    public Category() {}

    public Category(@Nullable String name, @Nullable String id, @Nullable URL url) {
        this.name = name ;
        this.id = id ;
        this.url = url ;
    }

    @Nullable
    public String getName() {
        return name ;
    }

    @Nullable
    public String getId() {
        return id ;
    }

    @Nullable
    public URL getUrl() {
        return url ;
    }
}
