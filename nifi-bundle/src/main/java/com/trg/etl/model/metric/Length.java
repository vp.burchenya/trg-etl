package com.trg.etl.model.metric ;

import org.jetbrains.annotations.NotNull ;


public class Length extends Metric {
    public Length(@NotNull Double value, @NotNull Unit unit) {
        super(value, unit) ;
    }
}
