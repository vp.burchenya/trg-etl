package com.trg.etl.model ;

import org.jetbrains.annotations.Nullable ;


public class Price {

    @Nullable
    private String currency ;
    @Nullable
    private Double original ;
    @Nullable
    private Double current ;

    public Price() {}

    public Price(@Nullable Double original, @Nullable Double current, @Nullable String currency) {
        this.original = original ;
        this.current = current ;
        this.currency = currency ;
    }

    @Nullable
    public String getCurrency() {
        return currency ;
    }
    public void setCurrency(@Nullable String currency) {
        this.currency = currency ;
    }

    @Nullable
    public Double getOriginal() {
        return original ;
    }
    public void setOriginal(@Nullable Double original) {
        this.original = original ;
    }

    @Nullable
    public Double getCurrent() {
        return current ;
    }
    public void setCurrent(@Nullable Double current) {
        this.current = current ;
    }
}
