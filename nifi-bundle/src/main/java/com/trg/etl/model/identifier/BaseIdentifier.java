package com.trg.etl.model.identifier ;

import org.jetbrains.annotations.NotNull ;
import java.util.Objects;


public class BaseIdentifier {
    private String value ;

    public BaseIdentifier(@NotNull String value) {
        this.value = value ;
    }

    public String getValue() {
        return value ;
    }

    public String toString() {
        return getValue() ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true ;
        if (o == null || getClass() != o.getClass()) return false ;
        BaseIdentifier that = (BaseIdentifier) o ;
        return getValue().equals(that.getValue()) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue()) ;
    }
}
