package com.trg.etl.model.metric ;

import org.jetbrains.annotations.NotNull ;

import java.util.Objects;


public class Metric {
    @NotNull
    private Double value ;
    @NotNull
    private Unit unit ;

    public Metric(@NotNull Double value, @NotNull Unit unit) {
        this.value = value ;
        this.unit = unit ;
    }

    @NotNull
    public Double getValue() {
        return value ;
    }
    public void setValue(@NotNull Double value) {
        this.value = value ;
    }

    @NotNull
    public Unit getUnit() {
        return unit ;
    }
    public void setUnit(@NotNull Unit unit) {
        this.unit = unit ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true ;
        if (o == null || getClass() != o.getClass()) return false ;
        Metric metric = (Metric) o ;
        return getValue().equals(metric.getValue()) &&
                getUnit() == metric.getUnit() ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue(), getUnit()) ;
    }
}
