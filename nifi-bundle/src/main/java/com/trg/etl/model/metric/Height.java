package com.trg.etl.model.metric ;

import org.jetbrains.annotations.NotNull;


public class Height extends Metric {
    public Height(@NotNull Double value, @NotNull Unit unit) {
        super(value, unit) ;
    }
}
