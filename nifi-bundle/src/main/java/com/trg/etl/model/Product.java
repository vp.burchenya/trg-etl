package com.trg.etl.model ;

import com.trg.etl.common.CaseInsensitiveMap ;
import com.trg.etl.model.identifier.BaseIdentifier ;

import com.trg.etl.model.metric.Dimensions ;
import org.jetbrains.annotations.Nullable ;

import java.util.UUID ;
import java.net.URL ;
import java.time.ZonedDateTime ;
import java.util.List ;
import java.util.Set ;


public class Product extends Entity {

    private ZonedDateTime createdAt ;
    private URL url ;
    private Condition condition ;
    private String provider ;
    private String marketplace ;
    private String title ;
    @Nullable private String shortDescription ;
    @Nullable private String longDescription ;
    private Set<BaseIdentifier> identifiers ;
    private CaseInsensitiveMap<List<String>> attributes ;
    private List<Category> primaryCategory ;
    private List<List<Category>> secondaryCategories ;
    @Nullable private URL mainImage ;
    private List<URL> subImages ;  // TODO: It is should be a nullable too
    private List<URL> stereoImages ;
    private List<URL> videos ;
    @Nullable private UUID productId ;
    private Price price ;
    private Dimensions dimensions ;
    private Dimensions shippingDimensions ;

    public ZonedDateTime getCreatedAt() {
        return createdAt ;
    }
    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt ;
    }

    public URL getUrl() {
        return url ;
    }
    public void setUrl(URL url) {
        this.url = url ;
    }

    public Condition getCondition() {
        return condition ;
    }
    public void setCondition(Condition condition) {
        this.condition = condition ;
    }

    public String getProvider() {
        return provider ;
    }
    public void setProvider(String provider) {
        this.provider = provider ;
    }

    public String getMarketplace() {
        return marketplace ;
    }
    public void setMarketplace(String marketplace) {
        this.marketplace = marketplace ;
    }

    public String getTitle() {
        return title ;
    }
    public void setTitle(String title) {
        this.title = title ;
    }

    @Nullable
    public String getShortDescription() {
        return shortDescription ;
    }
    public void setShortDescription(@Nullable String shortDescription) {
        this.shortDescription = shortDescription ;
    }

    @Nullable
    public String getLongDescription() {
        return longDescription ;
    }
    public void setLongDescription(@Nullable String longDescription) {
        this.longDescription = longDescription ;
    }

    public Set<BaseIdentifier> getIdentifiers() {
        return identifiers ;
    }
    public void setIdentifiers(Set<BaseIdentifier> identifiers) {
        this.identifiers = identifiers ;
    }

    public CaseInsensitiveMap<List<String>> getAttributes() {
        return attributes ;
    }
    public void setAttributes(CaseInsensitiveMap<List<String>> attributes) {
        this.attributes = attributes ;
    }

    public List<Category> getPrimaryCategory() {
        return primaryCategory ;
    }
    public void setPrimaryCategory(List<Category> primaryCategory) {
        this.primaryCategory = primaryCategory ;
    }

    public List<List<Category>> getSecondaryCategories() {
        return secondaryCategories ;
    }
    public void setSecondaryCategories(List<List<Category>> secondaryCategories) {
        this.secondaryCategories = secondaryCategories ;
    }

    @Nullable
    public URL getMainImage() {
        return mainImage ;
    }
    public void setMainImage(@Nullable URL mainImage) {
        this.mainImage = mainImage ;
    }

    public List<URL> getSubImages() {
        return subImages ;
    }
    public void setSubImages(List<URL> subImages) {
        this.subImages = subImages ;
    }

    public List<URL> getStereoImages() {
        return stereoImages ;
    }
    public void setStereoImages(List<URL> stereoImages) {
        this.stereoImages = stereoImages ;
    }

    public List<URL> getVideos() {
        return videos ;
    }
    public void setVideos(List<URL> videos) {
        this.videos = videos ;
    }

    @Nullable
    public UUID getProductId() {
        return productId ;
    }
    public void setProductId(@Nullable UUID productId) {
        this.productId = productId ;
    }

    public Price getPrice() {
        return price ;
    }
    public void setPrice(Price price) {
        this.price = price ;
    }

    public Dimensions getDimensions() {
        return dimensions ;
    }
    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions ;
    }

    public Dimensions getShippingDimensions() {
        return shippingDimensions ;
    }
    public void setShippingDimensions(Dimensions shippingDimensions) {
        this.shippingDimensions = shippingDimensions ;
    }
}
