package com.trg.etl.common ;


import org.jetbrains.annotations.NotNull ;

import java.util.HashMap ;


// TODO: test
public class CaseInsensitiveMap<V> extends HashMap<String, V> {

    @Override
    public V put(@NotNull String key, V value) {
        String newKey = key.toLowerCase() ;
        return super.put(newKey, value) ;
    }

    @Override
    public V get(@NotNull Object key) {
        String newKey = key.toString().toLowerCase() ;
        return super.get(newKey) ;
    }
}
