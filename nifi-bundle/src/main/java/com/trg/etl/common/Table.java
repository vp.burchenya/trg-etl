package com.trg.etl.common ;

import java.util.HashMap ;
import java.util.Map ;

import org.jetbrains.annotations.Nullable ;


public class Table<R, C, V> {

    protected final Map<R, Map<C, V>> backend ;
    private final Map<C, V> empty ;

    public Table() {
        backend = new HashMap<>() ;
        empty = new HashMap<>() ;
    }

    public void add(R t, C m, V v) {
        backend
        .computeIfAbsent(t, key -> new HashMap<>())
        .put(m, v) ;
    }

    @Nullable
    public V get(R r, C c) {
        return backend.getOrDefault(r, empty).get(c) ;
    }
}
