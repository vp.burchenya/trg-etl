package com.trg.etl.rr.to.product ;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties ;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.persistence.Id ;
import javax.validation.constraints.NotBlank ;


@JsonIgnoreProperties(ignoreUnknown = true)
public class MasterDocumentProductTo {
    @Id
    @NotBlank
    @JsonProperty("_id")
    private String objectId ;

    @JsonUnwrapped
    private ProductTo body ;

    public String getObjectId() {
        return objectId ;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId ;
    }

    public ProductTo getBody() {
        return body ;
    }
    public void setBody(ProductTo body) {
        this.body = body ;
    }
}
