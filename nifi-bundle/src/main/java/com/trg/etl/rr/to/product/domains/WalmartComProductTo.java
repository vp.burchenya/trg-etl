package com.trg.etl.rr.to.product.domains ;


import com.trg.etl.rr.to.product.ProductTo;

public class WalmartComProductTo extends ProductTo {
    @Override
    public String getBaseIdentifierTypeName() {
        return "sku" ;
    }

    /*
        TODO: Business logic validations
        - not instanceof NoNameProduct
        - getProductId() -> UUID
     */
}
