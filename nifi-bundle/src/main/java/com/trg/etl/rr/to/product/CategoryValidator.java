package com.trg.etl.rr.to.product ;

import javax.validation.Constraint ;
import javax.validation.Payload ;
import java.lang.annotation.* ;


@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CategoryValidatorImpl.class)
@Documented
public @interface CategoryValidator {
    String message() default "Broken category data" ;
    Class<?>[] groups() default {} ;
    Class<? extends Payload>[] payload() default {} ;
}
