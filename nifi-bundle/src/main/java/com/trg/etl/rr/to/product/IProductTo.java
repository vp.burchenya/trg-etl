package com.trg.etl.rr.to.product ;

import com.fasterxml.jackson.annotation.JsonSubTypes ;
import com.fasterxml.jackson.annotation.JsonTypeInfo ;
import com.trg.etl.rr.to.product.domains.* ;


@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    property = "provider",
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    visible = true,
    defaultImpl = NoNameProductTo.class
)
@JsonSubTypes({
    @JsonSubTypes.Type(value=WalmartComProductTo.class, name="walmart.com"),
    @JsonSubTypes.Type(value=BestBuyComProductTo.class, name="bestbuy.com"),
})
public interface IProductTo {}
