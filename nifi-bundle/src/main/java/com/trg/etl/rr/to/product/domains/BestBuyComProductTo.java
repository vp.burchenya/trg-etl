package com.trg.etl.rr.to.product.domains ;


import com.trg.etl.rr.to.product.ProductTo;

public class BestBuyComProductTo extends ProductTo {
    @Override
    public String getBaseIdentifierTypeName() {
        return "sku" ;
    }
}
