package com.trg.etl.rr.to.product ;

import javax.validation.Valid ;
import javax.validation.constraints.NotNull ;
import org.hibernate.validator.constraints.URL ;

import java.util.ArrayList ;
import java.util.List ;


public class Categories {
    @NotNull
    @Valid
    private final Category primary ;
    @NotNull
    private final List<@NotNull @Valid Category> secondary ;

    @CategoryValidator
    public static class Category {
        @NotNull
        private final List<@NotNull String> names ;
        @NotNull
        private final List<@NotNull String> ids ;
        @NotNull
        private final List<@NotNull @URL String> links ;

        public Category() {
            names = new ArrayList<>() ;
            ids = new ArrayList<>() ;
            links = new ArrayList<>() ;
        }

        public List<String> getNames() {
            return names ;
        }
        public List<String> getIds() {
            return ids ;
        }
        public List<String> getLinks() {
            return links ;
        }
    }

    public Categories() {
        primary = new Category() ;
        secondary = new ArrayList<>() ;
    }

    public Category getPrimary() {
        return primary ;
    }
    public List<Category> getSecondary() {
        return secondary ;
    }
}
