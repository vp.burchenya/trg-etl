package com.trg.etl.rr.to.product ;

import javax.validation.constraints.NotNull ;


public class Metric {
    @NotNull
    private Double value ;
    @NotNull
    private String unit ;

    public Metric() {}
    public Metric(Double value, String unit) {
        this.value = value ;
        this.unit = unit ;
    }

    public Double getValue() {
        return value ;
    }
    public void setValue(Double value) {
        this.value = value ;
    }

    public String getUnit() {
        return unit ;
    }
    public void setUnit(String unit) {
        this.unit = unit ;
    }
}
