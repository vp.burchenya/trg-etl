package com.trg.etl.rr.to.product ;

import javax.validation.ConstraintValidator ;
import javax.validation.ConstraintValidatorContext ;
import java.util.Arrays;
import java.util.Set ;
import java.util.stream.Collectors ;


public class CategoryValidatorImpl implements ConstraintValidator<CategoryValidator, Categories.Category> {
    @Override
    public boolean isValid(Categories.Category value, ConstraintValidatorContext context) {
        Integer[] sizes = { value.getNames().size(), value.getIds().size(), value.getLinks().size() } ;
        Set<Integer> s = Arrays.stream(sizes).filter(i -> i > 0).collect(Collectors.toSet()) ;
        return ! (s.size() > 1) ;
    }
}
