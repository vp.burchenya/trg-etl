package com.trg.etl.rr.to.product ;

import java.util.ArrayList ;
import java.util.List ;

import com.fasterxml.jackson.annotation.JsonProperty ;
import com.fasterxml.jackson.annotation.JsonFormat ;
import static com.fasterxml.jackson.annotation.JsonFormat.Feature.* ;

import javax.validation.constraints.NotNull ;


public class Description {
    @NotNull
    @JsonFormat(with = ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<String> _long ;
    @NotNull
    @JsonFormat(with = ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<String> _short ;
    @JsonProperty("extra_points")
    private List<String> extraPoints ;

    public Description() {
        _long = new ArrayList<>() ;
        _short = new ArrayList<>() ;
    }

    public Description(String _long, String _short) {
        this() ;
        if (_long != null) this._long.add(_long) ;
        if (_short != null) this._short.add(_short) ;
    }

    public List<String> getLong() {
        return _long ;
    }
    public void setLong(List<String> _long) {
        this._long = _long ;
    }

    public List<String> getShort() {
        return _short ;
    }
    public void setShort(List<String> _short) {
        this._short = _short ;
    }

    public List<String> getExtraPoints() {
        return extraPoints ;
    }
    public void setExtraPoints(List<String> extraPoints) {
        this.extraPoints = extraPoints ;
    }
}
