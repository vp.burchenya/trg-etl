package com.trg.etl.rr.to.product ;

import com.fasterxml.jackson.annotation.JsonFormat ;
import com.fasterxml.jackson.annotation.JsonProperty ;

import javax.validation.constraints.NotNull ;
import java.util.ArrayList ;
import java.util.List ;

import static com.fasterxml.jackson.annotation.JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY ;


public class ImageSet {
    @NotNull
    // @Size(max = 1)
    @JsonFormat(with = ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<String> main ;
    @JsonFormat(with = ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<String> sub ;
    @JsonProperty("360_degrees")
    @JsonFormat(with = ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<String> stereo ;
    @JsonFormat(with = ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<String> video ;

    public ImageSet() {
        main = new ArrayList<>() ;
        sub = new ArrayList<>() ;
        stereo = new ArrayList<>() ;
        video = new ArrayList<>() ;
    }

    public List<String> getMain() {
        return main ;
    }
    public void setMain(List<String> main) {
        this.main = main ;
    }

    public List<String> getSub() {
        return sub ;
    }
    public void setSub(List<String> sub) {
        this.sub = sub ;
    }

    public List<String> getStereo() {
        return stereo ;
    }
    public void setStereo(List<String> stereo) {
        this.stereo = stereo ;
    }

    public List<String> getVideo() {
        return video ;
    }
    public void setVideo(List<String> video) {
        this.video = video ;
    }
}
