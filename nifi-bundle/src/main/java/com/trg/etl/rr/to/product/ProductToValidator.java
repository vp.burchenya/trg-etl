package com.trg.etl.rr.to.product ;

import javax.validation.Constraint ;
import javax.validation.Payload ;
import java.lang.annotation.* ;


@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ProductToValidatorImpl.class)
@Documented
public @interface ProductToValidator {
    String message() default "Broken product to data" ;
    Class<?>[] groups() default {} ;
    Class<? extends Payload>[] payload() default {} ;
}
