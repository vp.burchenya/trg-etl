package com.trg.etl.rr.to.product ;

import org.jetbrains.annotations.Nullable ;


public abstract class ProductTo extends ABCProductTo {
    @Nullable
    public abstract String getBaseIdentifierTypeName() ;
}
