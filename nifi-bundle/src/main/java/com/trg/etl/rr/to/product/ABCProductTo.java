package com.trg.etl.rr.to.product ;

import java.time.ZonedDateTime ;
import java.util.Map ;
import java.util.List ;

import com.fasterxml.jackson.annotation.JsonProperty ;
import com.fasterxml.jackson.annotation.JsonFormat ;

import org.jetbrains.annotations.Nullable ;

import javax.validation.Valid ;
import javax.validation.constraints.NotNull ;
import javax.validation.constraints.NotBlank ;
import org.hibernate.validator.constraints.URL ;
import javax.validation.constraints.Pattern ;
import static javax.validation.constraints.Pattern.Flag.* ;


@Valid
@ProductToValidator
public abstract class ABCProductTo implements IProductTo {
    /*
    @Id @NotBlank @JsonProperty("_id")
    // @JacksonInject ObjectId
    private String objectId ;
    */
    @NotBlank
    private String version ;
    @URL @NotBlank
    private String url ;
    // TODO: Must be coupled with the `Conditions Enum`, common for `Entity Model` and `DTO` (SOLID)
    @NotBlank
    @Pattern(regexp = "new|used|refurbished", flags = {CASE_INSENSITIVE, UNICODE_CASE})
    private String condition ;
    @NotBlank
    @Pattern(regexp = "regular_product|bundle")
    private String type ;
    @NotNull
    // @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSX", timezone = "UTC")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX", timezone = "UTC")
    @JsonProperty("created_at")
    private ZonedDateTime createdAt ;
    @NotBlank
    private String provider ;
    @NotBlank
    private String marketplace ;
    @NotBlank
    private String title ;
    @NotNull
    @Valid
    private Description description ;
    // TODO: We need to change identifiers list in two places
    @NotNull
    private Map<@NotNull @Pattern(regexp = "upc|sku|item_number", flags = {CASE_INSENSITIVE, UNICODE_CASE}) String, @NotNull List<String>> identifiers ;
    @NotNull
    private Map<@NotNull String, @NotNull List<String>> attributes ;
    @NotNull
    @JsonProperty("additional_fields")
    private Map<@NotNull String, @Nullable Object> additionalFields ;
    @NotNull
    @Valid
    private Categories categories ;
    @NotNull
    @Valid
    @JsonProperty("image_set")
    private ImageSet imageSet ;
    @NotNull
    @Valid
    private Price price ;
    @NotNull
    private Map<@NotBlank @Pattern(regexp = "length|height|depth|width|weight") String, @Nullable @Valid Metric> dimensions ;
    @NotNull
    @JsonProperty("shipping_dimensions")
    private Map<@NotBlank @Pattern(regexp = "length|height|depth|width|weight") String, @Nullable @Valid Metric> shipping_dimensions ;

    public String getVersion() {
        return version ;
    }
    public void setVersion(String version) {
        this.version = version ;
    }

    public String getUrl() {
        return url ;
    }
    public void setUrl(String url) {
        this.url = url ;
    }

    public String getCondition() {
        return condition ;
    }
    public void setCondition(String condition) {
        this.condition = condition ;
    }

    public String getType() {
        return type ;
    }
    public void setType(String type) {
        this.type = type ;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt ;
    }
    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt ;
    }

    public String getProvider() {
        return provider ;
    }
    public void setProvider(String provider) {
        this.provider = provider ;
    }

    public String getMarketplace() {
        return marketplace ;
    }
    public void setMarketplace(String marketplace) {
        this.marketplace = marketplace ;
    }

    public String getTitle() {
        return title ;
    }
    public void setTitle(String title) { this.title = title ;}

    public Description getDescription() {
        return description ;
    }
    public void setDescription(final Description description) {
        this.description = description ;
    }

    public Map<String, List<String>> getIdentifiers() {
        return identifiers ;
    }
    public void setIdentifiers(Map<String, List<String>> identifiers) {
        this.identifiers = identifiers ;
    }

    public Map<String, List<String>> getAttributes() {
        return attributes ;
    }
    public void setAttributes(Map<String, List<String>> attributes) {
        this.attributes = attributes ;
    }

    public Map<String, Object> getAdditionalFields() {
        return additionalFields ;
    }
    public void setAdditionalFields(Map<String, @Nullable Object> additionalFields) {
        this.additionalFields = additionalFields ;
    }

    public Categories getCategories() {
        return categories ;
    }
    public void setCategories(Categories categories) {
        this.categories = categories ;
    }

    public ImageSet getImageSet() {
        return imageSet ;
    }
    public void setImageSet(ImageSet imageSet) {
        this.imageSet = imageSet ;
    }

    public Price getPrice() {
        return price ;
    }
    public void setPrice(Price price) {
        this.price = price ;
    }

    public Map<String, Metric> getDimensions() {
        return dimensions ;
    }
    public void setDimensions(Map<String, Metric> dimensions) {
        this.dimensions = dimensions ;
    }

    public Map<String, Metric> getShippingDimensions() {
        return dimensions ;
    }
    public void setShippingDimensions(Map<String, Metric> dimensions) {
        this.dimensions = dimensions ;
    }
}
