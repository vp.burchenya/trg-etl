package com.trg.etl.rr.to.product;


public class Price {

    private String currency ;
    private Double original ;
    private Double current ;

    public String getCurrency() {
        return currency ;
    }
    public void setCurrency(String currency) {
        this.currency = currency ;
    }

    public Double getOriginal() {
        return original ;
    }
    public void setOriginal(Double original) {
        this.original = original ;
    }

    public Double getCurrent() {
        return current ;
    }
    public void setCurrent(Double current) {
        this.current = current ;
    }
}
