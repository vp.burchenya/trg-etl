package com.trg.etl.nifi.processors ;

import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor ;
import org.apache.nifi.processor.ProcessSession ;
import org.apache.nifi.util.MockFlowFile ;

import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners ;

import org.junit.Test ;
import static org.junit.Assert.* ;


public class TestDummyProcessorImpl {
    @Test
    public void testDummyProcessor() {
        AbstractProcessor processor = new DummyProcessorImpl() ;
        TestRunner testRunner = TestRunners.newTestRunner(processor) ;  // or DummyProcessorImpl.class
        testRunner.setProperty("DUMMY_PROPERTY", "Im a teapot") ;

        ProcessSession session = testRunner.getProcessSessionFactory().createSession() ;
        FlowFile ff = session.create() ;
        // MockFlowFile _ff = testRunner.enqueue("payload".getBytes()) ;
        // testRunner.enqueue("payload".getBytes()) ;  // returns FlowFile
        ff = session.putAttribute(ff, "last_value", "1234") ;
        testRunner.enqueue(ff) ;
        testRunner.run() ;

        testRunner.assertQueueEmpty() ;
        testRunner.assertTransferCount(DummyProcessorImpl.REL_DUMMY, 1) ;

        for (MockFlowFile ffItem : testRunner.getFlowFilesForRelationship("DUMMY_REL")) {
            byte[] res = testRunner.getContentAsByteArray(ffItem) ;
            assertNotNull(res) ;
            ffItem.assertAttributeEquals("last_value", "1234Im a teapot1234") ;
        }
    }
}
