package com.trg.etl.util ;

import static com.trg.etl.util.ProductId.calculateProductId ;

import org.junit.* ;
import static org.junit.Assert.* ;

import java.util.UUID ;


public class TestProductId {
    UUID uuid ;

    @Test
    public void testCalculateProductId() throws Exception {
        uuid = calculateProductId("walmart.caN-11017855") ;
        assertEquals(4, uuid.version()) ;
        assertEquals(UUID.fromString("6f919093-64d1-4945-a3e5-46dceafdeff6"), uuid) ;

        uuid = calculateProductId("walmart.com123") ;
        assertEquals(4, uuid.version()) ;
        assertEquals(UUID.fromString("f10ca520-2aca-4cd4-9b07-965a31c8144f"), uuid) ;

        uuid = calculateProductId("") ;
        assertEquals(4, uuid.version()) ;
        assertEquals(UUID.fromString("d41d8cd9-8f00-4204-a980-0998ecf8427e"), uuid) ;
    }
}
