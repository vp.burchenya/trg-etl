package com.trg.etl.model.metric ;

import org.junit.* ;
import static org.junit.Assert.* ;


public class TestMetric {
    @Test
    public void testEqual() {
        Metric length = new Length(1.5, Unit.INCH) ;
        Metric width = new Width(1.5, Unit.INCH) ;

        assertNotEquals(length, null) ;
        assertNotEquals(length, width) ;
        assertNotEquals(new Metric(1.5, Unit.INCH), new Length(1.5, Unit.INCH)) ;
        assertNotEquals(length, new Length(0.5, Unit.INCH)) ;
        assertNotEquals(length, new Length(1.5, Unit.CM)) ;
        assertEquals(length, new Length(1.5, Unit.INCH)) ;
        assertEquals(new Metric(1.5, Unit.INCH), new Metric(1.5, Unit.INCH)) ;
    }
}
