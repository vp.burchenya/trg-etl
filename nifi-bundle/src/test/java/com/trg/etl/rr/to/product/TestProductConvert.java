package com.trg.etl.rr.to.product ;

import com.trg.etl.mapping.IProductMapper ;
import com.trg.etl.mapping.error.* ;

import com.trg.etl.model.Product ;
import com.trg.etl.model.Condition ;
import com.trg.etl.model.identifier.* ;
import com.trg.etl.model.metric.* ;

import java.net.URL ;
import java.time.ZoneId ;
import java.time.ZonedDateTime ;
import java.util.* ;

import com.trg.etl.rr.to.product.domains.WalmartComProductTo;
import org.junit.* ;
import static org.junit.Assert.* ;


public class TestProductConvert {

    private ProductTo to ;
    private final IProductMapper mapper = IProductMapper.INSTANCE ;

    @Before
    public void createProductTo() {
        to = new WalmartComProductTo() ;

        to.setCreatedAt(ZonedDateTime.now(ZoneId.of("UTC"))) ;
        to.setUrl("http://walmart.com/cat/78019?v=1") ;
        to.setCondition("new") ;
        to.setTitle("Vitek VT28401 Microwave") ;
        to.setProvider("walmart.com") ;
        to.setMarketplace("walmart.com") ;
        to.setDescription(new Description("Vitek VT28401 Microwave with MasterChef feature", null)) ;
        to.setIdentifiers(new HashMap<String, List<String>>() {{
            put("upc", Collections.singletonList("123456789012")) ;
            put("sku", Collections.singletonList("N-11017855")) ;
        }}) ;
        to.setAttributes(new HashMap<String, List<String>>() {{
            put("Brand", Collections.singletonList("Vitek")) ;
            put("Color", Arrays.asList("White", "silver")) ;
        }}) ;
        Categories categories = new Categories() ;
        categories.getPrimary().getNames().add("Electronics") ;
        categories.getPrimary().getNames().add("Microwave") ;
        categories.getPrimary().getIds().add("11") ;
        categories.getPrimary().getIds().add("48") ;
        Categories.Category secondaryCategory = new Categories.Category() ;
        secondaryCategory.getNames().add("AllMarket") ;
        categories.getSecondary().add(secondaryCategory) ;
        to.setCategories(categories) ;

        to.setImageSet(new ImageSet()) ;
        to.getImageSet().getMain().add("http://1.gif") ;
        to.getImageSet().getMain().add("http://10.gif") ;
        to.getImageSet().getVideo().add("http://1.avi") ;
        to.getImageSet().getVideo().add("http://10.avi") ;

        Price price = new Price() ;
        price.setCurrent(15.99) ;
        to.setPrice(price) ;

        to.setDimensions(new HashMap<String, Metric>() {{
            put("length", new Metric(1.5, "inh")) ;
            put("weight", new Metric(10.14, "lbr")) ;
        }}) ;
    }

    @Test
    public void testToConvert() throws Exception {
        Product model = mapper.toModel(to) ;

        assertEquals("Vitek VT28401 Microwave with MasterChef feature", model.getLongDescription()) ;
        assertNull(model.getShortDescription()) ;
        assertEquals(ZoneId.of("UTC"), model.getCreatedAt().getZone()) ;
        assertEquals(model.getCreatedAt(), to.getCreatedAt()) ;
        assertEquals("http://walmart.com/cat/78019?v=1", model.getUrl().toString()) ;
        assertSame(Condition.NEW, model.getCondition()) ;
        assertEquals(model.getTitle(), to.getTitle()) ;
        assertEquals(model.getProvider(), to.getProvider()) ;
        assertEquals(model.getMarketplace(), to.getMarketplace()) ;
        assertArrayEquals(
            new BaseIdentifier[] { new UPC("123456789012"), new SKU("N-11017855") },
            model.getIdentifiers().toArray()
        ) ;
        assertArrayEquals(
            new String[] { "Vitek" },
            model.getAttributes().get("Brand").toArray()
        ) ;
        assertArrayEquals(
            new String[] { "White", "silver" },
            model.getAttributes().get("color").toArray()
        ) ;

        assertSame(model.getAttributes().get("color"), model.getAttributes().get("COLOR")) ;

        assertEquals(2, model.getPrimaryCategory().size()) ;
        assertEquals("11", model.getPrimaryCategory().get(0).getId()) ;
        assertEquals("Electronics", model.getPrimaryCategory().get(0).getName()) ;
        assertNull(model.getPrimaryCategory().get(0).getUrl()) ;
        assertEquals("48", model.getPrimaryCategory().get(1).getId()) ;
        assertEquals("Microwave", model.getPrimaryCategory().get(1).getName()) ;
        assertNull(model.getPrimaryCategory().get(1).getUrl()) ;
        assertEquals("AllMarket", model.getSecondaryCategories().get(0).get(0).getName()) ;

        assertEquals(new URL("http://1.gif"), model.getMainImage()) ;
        assertEquals(1, model.getSubImages().size()) ;
        assertEquals(new URL("http://10.gif"), model.getSubImages().get(0)) ;
        assertArrayEquals(
            new URL[] {new URL("http://1.avi"), new URL("http://10.avi")},
            model.getVideos() != null ? model.getVideos().toArray() : new URL[] {}
        ) ;
        assertNotNull(model.getStereoImages()) ;

        assertEquals(UUID.fromString("09c383d6-6d16-4d5c-9f2b-f2c26842908f"), model.getProductId()) ;

        assertNull(model.getPrice().getCurrency()) ;
        assertEquals(Double.valueOf(15.99), model.getPrice().getCurrent()) ;

        assertEquals(new Length(1.5, Unit.INCH), model.getDimensions().getLength()) ;
        assertEquals(new Weight(10.14, Unit.POUND), model.getDimensions().getWeight()) ;
        assertNull(model.getDimensions().getWidth()); ;

        System.out.println(model.getShippingDimensions()) ;
    }

    @Test
    public void testNullProductId() throws Exception {
        to.getIdentifiers().remove("sku") ;
        Product model = mapper.toModel(to) ;
        assertNull(model.getProductId()) ;
    }

    @Test(expected = UnknownIdentifierType.class)
    public void testUnknownIdentifier() throws Exception {
        to.setIdentifiers(new HashMap<String, List<String>>() {{
            put("", Collections.singletonList("123456789012")) ;
        }}) ;
        // assertThrows(UnknownIdentifierType.class, () -> mapper.toModel(to)) ;
        mapper.toModel(to) ;
    }

    @Test(expected = MultipleBaseIdentifiers.class)
    public void testMultipleBaseIdentifiers() throws Exception {
        to.setIdentifiers(new HashMap<String, List<String>>() {{
            put("sku", Arrays.asList("1-0010-1234-00-3", "N-11017855")) ;
        }}) ;
        mapper.toModel(to) ;
    }
}
