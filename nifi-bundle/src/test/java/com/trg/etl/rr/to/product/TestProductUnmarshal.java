package com.trg.etl.rr.to.product ;

import org.junit.* ;

import javax.validation.Validator ;
import javax.validation.Validation ;
import javax.validation.ConstraintViolation ;

import com.fasterxml.jackson.databind.ObjectMapper ;
import com.fasterxml.jackson.databind.json.JsonMapper ;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module ;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule ;
import com.fasterxml.jackson.databind.DeserializationFeature ;
import com.fasterxml.jackson.databind.SerializationFeature ;
import com.fasterxml.jackson.databind.JsonMappingException ;
import com.fasterxml.jackson.core.JsonParseException ;

import java.io.IOException ;
import java.io.InputStream ;
import java.util.Set ;
import java.time.ZoneId ;
import java.time.ZonedDateTime ;

import static org.junit.Assert.* ;

import com.trg.rr.assets.product.AssetsLoader ;
import com.trg.rr.assets.product.com.walmart.WalmartComAssetsLoader ;
import com.trg.rr.assets.product.com.bestbuy.BestBuyComAssetsLoader ;

import com.trg.etl.rr.to.product.domains.* ;


public class TestProductUnmarshal {

    private static AssetsLoader WalmartComAssetsLoader, BestBuyComAssetsLoader ;
    private InputStream walmartComAssetInputStream, bestBuyComAssetInputStream ;

    private ObjectMapper jsonMapper ;
    private static Validator validator ;

    @BeforeClass
    public static void setupClass() {
        WalmartComAssetsLoader = new WalmartComAssetsLoader() ;
        BestBuyComAssetsLoader = new BestBuyComAssetsLoader() ;
    }

    @BeforeClass
    public static void setupValidator() {
        validator = Validation.buildDefaultValidatorFactory().getValidator() ;
    }

    @Before
    public void loadWalmartComAsset() {
        walmartComAssetInputStream = WalmartComAssetsLoader.loadAsset("product-0.json") ;
        assertNotNull(walmartComAssetInputStream) ;
    }

    @Before
    public void loadBestBuyComAsset() {
        bestBuyComAssetInputStream = BestBuyComAssetsLoader.loadAsset("product-0.json") ;
        assertNotNull(bestBuyComAssetInputStream) ;
    }

    @Before
    public void setupJsonMapper() {
        // ObjectMapper mapper = new ObjectMapper() ;
        jsonMapper = JsonMapper.builder()
        .addModule(new Jdk8Module())
        .addModule(new JavaTimeModule())
        .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
        .enable(SerializationFeature.INDENT_OUTPUT)  // mapper.writeValueAsString(product)
        .build() ;
    }

    @Test
    public void testNoNameProductLoad() throws IOException {
        assertTrue(jsonMapper.readValue(
            WalmartComAssetsLoader.loadAsset("product-noname.json"), ProductTo.class)
            instanceof NoNameProductTo
        ) ;
    }

    @Test
    public void testWalmartComProductLoads() throws IOException {
        ProductTo product ;
        // noinspection CaughtExceptionImmediatelyRethrown
        try {
            product = jsonMapper.readValue(walmartComAssetInputStream, ProductTo.class) ;
            Set<ConstraintViolation<ProductTo>> errors = validator.validate(product) ;
            errors.forEach(e -> System.err.printf("Validation error [%s]: %s\n", e.getPropertyPath(), e.getMessage())) ;
            assertEquals(0, errors.size()) ;
        } catch (JsonParseException | JsonMappingException e) {
            throw e ;
        }

        assertTrue(product instanceof WalmartComProductTo) ;

        assertEquals("2020-01-01", product.getVersion()) ;
        assertEquals("http://walmart.com/cat/11781?", product.getUrl()) ;
        assertEquals("New", product.getCondition()) ;
        assertEquals("regular_product", product.getType()) ;
        assertEquals(
            ZoneId.of("UTC"),
            product.getCreatedAt().getZone()
        ) ;
        assertEquals(
            ZonedDateTime.of(
                2020,
                1,
                1,
                14,
                38,
                47,
                123000000,
                ZoneId.of("UTC")
            ),
            product.getCreatedAt().withZoneSameInstant(ZoneId.of("UTC"))
        ) ;

        assertEquals("walmart.com", product.getProvider()) ;
        assertEquals("walmart.com", product.getMarketplace()) ;
        assertEquals(
            "Vitek VT28401 Microwave",
            product.getTitle()
        ) ;
        assertArrayEquals(
            new String[] { "Vitek VT28401 SMART Microwave with MasterChef function" },
            product.getDescription().getLong().toArray()
        ) ;
        assertArrayEquals(
            new String[] { "Vitek VT28401 SMART Microwave" },
            product.getDescription().getShort().toArray()
        ) ;
        assertNull(product.getDescription().getExtraPoints()) ;
        assertArrayEquals(
            new String[] { "sku", "upc" },
            product.getIdentifiers().keySet().toArray()
        ) ;
        assertArrayEquals(
            new String[] { "1-0010-1234-00-3" },
            product.getIdentifiers().get("sku").toArray()
        ) ;
        assertArrayEquals(
            new String[] { "123456789012" },
            product.getIdentifiers().get("upc").toArray()
        ) ;

        assertArrayEquals(
            new String[] { "Brand", "Model", "Color", "Features" },
            product.getAttributes().keySet().toArray()
        ) ;

        assertArrayEquals(
            new String[] { "Vitek" },
            product.getAttributes().get("Brand").toArray()
        ) ;
        assertArrayEquals(
            new String[] { "VT28401" },
            product.getAttributes().get("Model").toArray()
        ) ;
        assertArrayEquals(
            new String[] { "White", "silver" },
            product.getAttributes().get("Color").toArray()
        ) ;
        assertArrayEquals(
            new String[] { "MasterChef", "Smart" },
            product.getAttributes().get("Features").toArray()
        ) ;

        // We do not test unnecessary and dynamic AdditionalFields property
        assertArrayEquals(
            new String[] { "tags", "price_info", "rate", "ratings" },
            product.getAdditionalFields().keySet().toArray()
        ) ;

        // TODO: test categories

        product.getCategories().getPrimary().getNames().add("123") ;
        assertEquals(1, validator.validate(product).size()) ;

        assertEquals(0, product.getImageSet().getSub().size()) ;
        assertEquals(0, product.getImageSet().getStereo().size()) ;
        assertEquals(0, product.getImageSet().getVideo().size()) ;
        assertArrayEquals(
            new String[] { "http://1.gif" },
            product.getImageSet().getMain().toArray()
        ) ;

        assertNull("USD", product.getPrice().getCurrency()) ;
        assertEquals(Double.valueOf(15.99), product.getPrice().getCurrent()) ;

        assertNull(product.getDimensions().get("height")) ;
        assertNull(product.getDimensions().get("length")) ;
        assertEquals("lbr", product.getDimensions().get("weight").getUnit()) ;
        assertEquals(Double.valueOf(14.5), product.getDimensions().get("weight").getValue()) ;

        assertNull(product.getShippingDimensions().get("length")) ;
    }

    @Test
    public void testBestBuyComProductLoads() throws IOException {
        ProductTo product;
        // noinspection CaughtExceptionImmediatelyRethrown
        try {
            product = jsonMapper.readValue(bestBuyComAssetInputStream, ProductTo.class);
            Set<ConstraintViolation<ProductTo>> errors = validator.validate(product);
            errors.forEach(e -> System.err.printf("Validation error [%s]: %s\n", e.getPropertyPath(), e.getMessage()));
            assertEquals(0, errors.size());
        } catch (JsonParseException | JsonMappingException e) {
            throw e;
        }

        assertTrue(product instanceof BestBuyComProductTo) ;

        assertEquals(0, product.getImageSet().getStereo().size()) ;
        assertEquals(8, product.getImageSet().getMain().size()) ;
        assertEquals(0, product.getImageSet().getSub().size()) ;
        assertEquals(3, product.getImageSet().getVideo().size()) ;
    }

    @Test
    public void testWalmartComMasterProductLoads() throws IOException {
        MasterDocumentProductTo product;
        // noinspection CaughtExceptionImmediatelyRethrown
        try {
            product = jsonMapper.readValue(WalmartComAssetsLoader.loadAsset("product-master-0.json"), MasterDocumentProductTo.class);
            Set<ConstraintViolation<MasterDocumentProductTo>> errors = validator.validate(product);
            errors.forEach(e -> System.err.printf("Validation error [%s]: %s\n", e.getPropertyPath(), e.getMessage()));
            assertEquals(0, errors.size());
        } catch (JsonParseException | JsonMappingException e) {
            throw e;
        }

        assertEquals("0123456789az0123456789xy", product.getObjectId()) ;
        assertEquals(Double.valueOf(15.99), product.getBody().getPrice().getCurrent()) ;
        assertTrue(product.getBody() instanceof WalmartComProductTo) ;
    }
}
