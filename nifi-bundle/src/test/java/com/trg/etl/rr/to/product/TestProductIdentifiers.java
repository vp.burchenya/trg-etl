package com.trg.etl.rr.to.product ;

import com.trg.etl.model.identifier.* ;
import org.junit.* ;
import static org.junit.Assert.* ;

public class TestProductIdentifiers {
    static class UpcX extends UPC {
        public UpcX(String value) {
            super(value) ;
        }
    }

    @Test
    public void testCompare() {
        assertEquals(new UPC("123456789010"), new UPC("123456789010")) ;
        assertNotEquals(new UPC("123456789010"), new UpcX("123456789010")) ;
        assertNotEquals(new UPC("123456789010"), new SKU("123456789010")) ;
    }
}
