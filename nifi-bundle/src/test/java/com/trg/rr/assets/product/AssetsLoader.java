package com.trg.rr.assets.product ;

import java.io.InputStream ;


public class AssetsLoader {
    private static Class<?> BASE_ASSET_LOADER = AssetsLoader.class ;

    public InputStream loadAsset(String ... name) {
        return BASE_ASSET_LOADER.getResourceAsStream(String.join("/", name)) ;
    }

    public InputStream loadAsset(String name) {
        return getClass().getResourceAsStream(name) ;
    }
}
